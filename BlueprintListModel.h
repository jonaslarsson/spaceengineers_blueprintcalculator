#ifndef BLUEPRINTLISTMODEL_H
#define BLUEPRINTLISTMODEL_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QDir>
#include <QString>
#include <QHash>
#include <QAbstractListModel>

class BlueprintListModel : public QAbstractListModel
{
    Q_OBJECT
    enum DataRole {
        NameRole = Qt::UserRole,
        ImageUrlRole,
        BlueprintFileRole
    };
    struct Blueprint;
    QList<Blueprint*> m_blueprints;
    QMap<QString, Blueprint*> m_blueprintsLocal;
    QDir m_blueprintDir;

public:
    BlueprintListModel();
    ~BlueprintListModel();
    bool refresh();
    QHash<int, QByteArray> roleNames() const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    void clear();
    void addLocalBlueprint(QString blueprintName);
};

#endif // BLUEPRINTLISTMODEL_H
