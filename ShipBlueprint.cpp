#include <QDebug>
#include <QStringList>
#include <QList>
#include <QFile>
#include <QXmlStreamReader>
#include "ShipBlueprint.h"

ShipBlueprint::ShipBlueprint()
{

}

ShipBlueprint::~ShipBlueprint()
{

}

void ShipBlueprint::setXmlData(const QByteArray &data)
{
    m_cubeBlocks.clear();
    QXmlStreamReader xml(data);
    QStringList xmlPathParts;
    QString xmlPath;

    while (!xml.atEnd())
    {
        switch (xml.readNext())
        {
        case QXmlStreamReader::StartElement:
            xmlPathParts.append(xml.name().toString());
            xmlPath = xmlPathParts.join(".");
            break;
        case QXmlStreamReader::Characters:
            if (xmlPath == "Definitions.ShipBlueprints.ShipBlueprint.CubeGrids.CubeGrid.CubeBlocks.MyObjectBuilder_CubeBlock.SubtypeName")
            {
                QString blockName = xml.text().toString();
                m_cubeBlocks[blockName] = m_cubeBlocks.value(blockName, 0) + 1;
            }
            break;
        case QXmlStreamReader::EndElement:
            xmlPathParts.pop_back();
            xmlPath = xmlPathParts.join(".");
            break;
        }
    }

    if (xml.hasError())
    {
        qDebug() << "Parse error";
    }
}

QMap<QString, int> ShipBlueprint::blocks() const
{
    return m_cubeBlocks;
}
