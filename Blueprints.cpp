#include <QDebug>
#include <QStringList>
#include <QXmlStreamReader>
#include "Blueprints.h"

struct Prerequisite
{
    Prerequisite()
    {
        amount = 0.0;
    }

    double amount;
    QString typeId;
    QString subtypeId;
};

struct Blueprints::Blueprint
{
    Blueprint()
    {
        result.amount = 0.0;
    }

    QString typeId;
    QString subtypeId;
    QString displayName;
    QList<Prerequisite> prerequisites;
    struct
    {
        double amount;
        QString typeId;
        QString subtypeId;
    } result;
};

Blueprints::Blueprints()
{

}

Blueprints::~Blueprints()
{
    clear();
}

void Blueprints::setXmlData(const QByteArray &data)
{
    QXmlStreamReader xml(data);
    QStringList xmlPathParts;
    QString xmlPath;
    Blueprint *blueprint = 0;
    clear();

    while (!xml.atEnd())
    {
        switch (xml.readNext())
        {
        case QXmlStreamReader::StartElement:
            xmlPathParts.append(xml.name().toString());
            xmlPath = xmlPathParts.join(".");
            if (xmlPath == "Definitions.Blueprints.Blueprint")
            {
                blueprint = new Blueprint;
            }
            else if (xmlPath == "Definitions.Blueprints.Blueprint.Prerequisites.Item")
            {
                Prerequisite prerequisite;
                prerequisite.amount = xml.attributes().value("Amount").toDouble();
                prerequisite.typeId = xml.attributes().value("TypeId").toString();
                prerequisite.subtypeId = xml.attributes().value("SubtypeId").toString();
                blueprint->prerequisites.append(prerequisite);
            }
            else if (xmlPath == "Definitions.Blueprints.Blueprint.Result")
            {
                blueprint->result.amount = xml.attributes().value("Amount").toDouble();
                blueprint->result.typeId = xml.attributes().value("TypeId").toString();
                blueprint->result.subtypeId = xml.attributes().value("SubtypeId").toString();
            }
            break;
        case QXmlStreamReader::Characters:
            if (xmlPath == "Definitions.Blueprints.Blueprint.Id.TypeId")
            {
                blueprint->typeId = xml.text().toString();
            }
            else if (xmlPath == "Definitions.Blueprints.Blueprint.Id.SubtypeId")
            {
                blueprint->subtypeId = xml.text().toString();
            }
            break;
        case QXmlStreamReader::EndElement:
            if (xmlPath == "Definitions.Blueprints.Blueprint")
            {
                //debugBlueprint(blueprint);
                m_blueprints.append(blueprint);
                if (blueprint->result.typeId == "Component")
                {
                    m_components.insert(blueprint->result.subtypeId, blueprint);
                }

                blueprint = 0;
            }
            xmlPathParts.pop_back();
            xmlPath = xmlPathParts.join(".");
            break;
        }

    }

    if (xml.hasError())
    {
        qDebug() << "Parse error";
    }
}

QMap<QString, double> Blueprints::requiredIngots(const QString &componentName)
{
    QMap<QString, double> ingotList;
    if (m_components.contains(componentName))
    {
        Blueprint *blueprint = m_components.value(componentName);
        for (int i = 0; i < blueprint->prerequisites.count(); i++)
        {
            if (blueprint->prerequisites.at(i).typeId == "Ingot")
            {
                ingotList.insert(blueprint->prerequisites.at(i).subtypeId,
                                 blueprint->prerequisites.at(i).amount);
            }
        }
    }
    return ingotList;
}

void Blueprints::clear()
{
    foreach (Blueprint *blueprint, m_blueprints)
    {
        delete blueprint;
    }
    m_blueprints.clear();
    m_components.clear();
}

void Blueprints::debugBlueprint(Blueprints::Blueprint *blueprint)
{
    qDebug() << "BLUEPRINT" << blueprint->typeId
             << blueprint->subtypeId;
    qDebug() << "    prerequisites";
    for (int i = 0; i < blueprint->prerequisites.count(); i++)
    {
        qDebug() << "        amount" << blueprint->prerequisites.at(i).amount
                 << "typeId" << blueprint->prerequisites.at(i).typeId
                 << "subtypeId" << blueprint->prerequisites.at(i).subtypeId;
    }
    qDebug() << "    results";
    qDebug() << "        amount" << blueprint->result.amount
             << "typeId" << blueprint->result.typeId
             << "subtypeId" << blueprint->result.subtypeId;
}
