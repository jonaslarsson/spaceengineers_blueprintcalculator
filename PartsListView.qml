import QtQuick 2.0

Item {
    width: 200
    id: root
    property alias model: repeater.model

    function fixNumber(number)
    {
        return number
        //return number.toFixed(1)
    }

    Column {
        anchors.fill: parent
        Repeater {
            id: repeater
            delegate: Rectangle {
                height: 20
                width: 199
                color: "green"
                Text {
                    anchors.fill: parent
                    anchors.leftMargin: 10
                    text: root.model[index].name
                }
                Text {
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    text: fixNumber(root.model[index].amount)
                    horizontalAlignment: Text.AlignRight
                }
            }
        }
    }
}
