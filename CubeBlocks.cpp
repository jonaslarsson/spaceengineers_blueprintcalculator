#include <QDebug>
#include <QStringList>
#include <QList>
#include <QXmlStreamReader>
#include "CubeBlocks.h"

struct Component
{
    QString subType;
    int count;
};

struct CubeBlocks::Definition
{
    QString typeId;
    QString subtypeId;
    QString cubeSize;
    QList<Component> components;
};

CubeBlocks::CubeBlocks()
{

}

CubeBlocks::~CubeBlocks()
{
    clear();
}

void CubeBlocks::setXmlData(const QByteArray &data)
{
    QXmlStreamReader xml(data);
    QStringList xmlPathParts;
    QString xmlPath;
    Definition *definition = 0;
    clear();

    while (!xml.atEnd())
    {
        switch (xml.readNext())
        {
        case QXmlStreamReader::StartElement:
            xmlPathParts.append(xml.name().toString());
            xmlPath = xmlPathParts.join(".");
            if (xmlPath == "Definitions.CubeBlocks.Definition")
            {
                definition = new Definition;
            }
            else if (xmlPath == "Definitions.CubeBlocks.Definition.Components.Component")
            {
                Component component;
                component.subType = xml.attributes().value("Subtype").toString();
                component.count = xml.attributes().value("Count").toInt();
                definition->components.append(component);
            }
            break;
        case QXmlStreamReader::Characters:
            if (xmlPath == "Definitions.CubeBlocks.Definition.Id.TypeId")
            {
                definition->typeId = xml.text().toString();
            }
            else if (xmlPath == "Definitions.CubeBlocks.Definition.Id.SubtypeId")
            {
                definition->subtypeId = xml.text().toString();
            }
            else if (xmlPath == "Definitions.CubeBlocks.Definition.CubeSize")
            {
                definition->cubeSize = xml.text().toString();
            }
            break;
        case QXmlStreamReader::EndElement:
            if (xmlPath == "Definitions.CubeBlocks.Definition")
            {
                m_definitions.append(definition);
                m_blocks.insert(definition->subtypeId, definition);
                definition = 0;
            }
            xmlPathParts.pop_back();
            xmlPath = xmlPathParts.join(".");
            break;
        }

    }

    if (xml.hasError())
    {
        qDebug() << "Parse error";
    }
}

QMap<QString, int> CubeBlocks::requiredComponents(const QString &blockName) const
{
    QMap<QString, int> componentList;
    if (m_blocks.contains(blockName))
    {
        Definition *definition = m_blocks.value(blockName);
        for (int i = 0; i < definition->components.count(); i++)
        {
            QString name = definition->components.at(i).subType;
            int count = definition->components.at(i).count;
            count += componentList.value(name, 0);
            componentList.insert(name, count);
        }
    }
    return componentList;
}

void CubeBlocks::clear()
{
    foreach (Definition *definition, m_definitions)
    {
        delete definition;
    }
    m_definitions.clear();
    m_blocks.clear();
}
