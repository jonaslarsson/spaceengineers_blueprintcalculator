#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlContext>
#include <QFile>
#include "BlueprintListModel.h"
#include "ShipInfo.h"

/*

Materials Produced == Ore x (RefineryEfficiency x 1.0905077 ^ EffModules)

Ore                = the specific Ore's Material Efficiency (Ore-Ingot Ratio)
RefineryEfficiency = the Refinery's Material Efficiency (which for the Refinery is 0.8)
EffModules         = the amount of attachment point connected Effectiveness Modules,
                     each module fills 2 attachment ports - so provides 2 each

Arc Furnace Material Efficiency 0.9
 - Iron
 - Nickel
 - Cobalt
Refinery Material Efficiency 0.8

http://www.spaceengineerswiki.com/Refinery#Efficiency

Ore-Ingot Ratio
Stone         90
Iron Ore      70
Silicon Ore   70
Nickel Ore    40
Cobalt Ore    30
Silver Ore    10
Gold Ore      1
Magnesium Ore 0.7
Uranium Ore   0.7
Platinum Ore  0.

Components


C:\Program Files (x86)\Steam\steamapps\common\SpaceEngineers\Content\Data

Components.sbc
Names of components
Mass
Volume

Blueprints.sbc
Component required materials
Ore conversion ratio

CubeBlocks.sbc
Components per block


-----------------

bp.spc

    <MyObjectBuilder_CubeBlock xsi:type="MyObjectBuilder_Reactor">
    <SubtypeName>SmallBlockLargeGenerator</SubtypeName>

CubeBlocks.sbc

    <Definition xsi:type="MyObjectBuilder_ReactorDefinition">
      <Id>
        <TypeId>Reactor</TypeId>
        <SubtypeId>SmallBlockLargeGenerator</SubtypeId>
      </Id>
      <Components>
        <Component Subtype="SteelPlate" Count="40" />
        <Component Subtype="Construction" Count="9" />
        <Component Subtype="MetalGrid" Count="9" />
        <Component Subtype="LargeTube" Count="3" />
        <Component Subtype="Reactor" Count="95" />
        <Component Subtype="Motor" Count="5" />
        <Component Subtype="Computer" Count="25" />
        <Component Subtype="SteelPlate" Count="20" />
      </Components>

Blueprints.sbc

    <Blueprint>
      <Id>
        <TypeId>BlueprintDefinition</TypeId>
        <SubtypeId>SteelPlate</SubtypeId>
      </Id>
      <DisplayName>DisplayName_Item_SteelPlate</DisplayName>
      <Icon>Textures\GUI\Icons\component\steel_plate_component.dds</Icon>
      <Prerequisites>
        <Item Amount="21" TypeId="Ingot" SubtypeId="Iron" />
      </Prerequisites>
      <Result Amount="1" TypeId="Component" SubtypeId="SteelPlate" />
      <BaseProductionTimeInSeconds>1</BaseProductionTimeInSeconds>
    </Blueprint>

    <Blueprint>
      <Id>
        <TypeId>BlueprintDefinition</TypeId>
        <SubtypeId>ConstructionComponent</SubtypeId>
      </Id>
      <DisplayName>DisplayName_Item_ConstructionComponent</DisplayName>
      <Icon>Textures\GUI\Icons\component\construction_components_component.dds</Icon>
      <Prerequisites>
        <Item Amount="10" TypeId="Ingot" SubtypeId="Iron" />
      </Prerequisites>
      <Result Amount="1" TypeId="Component" SubtypeId="Construction" />
      <BaseProductionTimeInSeconds>4</BaseProductionTimeInSeconds>
    </Blueprint>

*/

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    ShipInfo shipInfo;

    shipInfo.setBlueprintsFile("C:/Program Files (x86)/Steam/steamapps/common/SpaceEngineers/Content/Data/Blueprints.sbc");
    shipInfo.setCubeBlocksFile("C:/Program Files (x86)/Steam/steamapps/common/SpaceEngineers/Content/Data/CubeBlocks.sbc");

    BlueprintListModel blueprintListModel;
    blueprintListModel.refresh();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("BlueprintModel", &blueprintListModel);
    engine.rootContext()->setContextProperty("ShipInfo", &shipInfo);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
