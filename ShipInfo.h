#ifndef SHIPINFO_H
#define SHIPINFO_H

#include <QObject>
#include <QMap>
#include <QString>
#include <QVariantMap>
#include "Blueprints.h"
#include "CubeBlocks.h"
#include "ShipBlueprint.h"

class ShipInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList blocks READ blocks NOTIFY shipLoaded)
    Q_PROPERTY(QVariantList parts READ parts NOTIFY shipLoaded)

public:
    explicit ShipInfo(QObject *parent = 0);
    void setCubeBlocksFile(const QString& filename);
    void setBlueprintsFile(const QString& filename);

    QVariantList blocks() const;
    QVariantList parts() const;

public slots:
    bool setShipFile(const QString& filename);

signals:
    void shipLoaded();

private:
    Blueprints m_blueprints;
    CubeBlocks m_cubeBlocks;
    ShipBlueprint m_shipBlueprint;
    QVariantMap m_blocks;
};

#endif // SHIPINFO_H
