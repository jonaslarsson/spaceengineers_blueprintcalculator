#include <QFile>
#include <QDebug>
#include "ShipInfo.h"

ShipInfo::ShipInfo(QObject *parent) : QObject(parent)
{

}

void ShipInfo::setCubeBlocksFile(const QString &filename)
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        m_cubeBlocks.setXmlData(file.readAll());
        file.close();
    }
}

void ShipInfo::setBlueprintsFile(const QString &filename)
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        m_blueprints.setXmlData(file.readAll());
        file.close();
    }
}

QVariantList ShipInfo::blocks() const
{
    QMap<QString, int> blocks = m_shipBlueprint.blocks();
    QVariantList list;
    foreach (QString blockName, blocks.keys())
    {
        QVariantMap block;
        block["name"] = blockName;
        block["amount"] = blocks.value(blockName);
        list.append(block);
    }
    return list;
}

QVariantList ShipInfo::parts() const
{
    QMap<QString, int> blocks = m_shipBlueprint.blocks();
    QMap<QString, int> totalComponents;
    foreach (QString blockName, blocks.keys())
    {
        int blockCount = blocks.value(blockName);
        QMap<QString, int> components = m_cubeBlocks.requiredComponents(blockName);
        foreach (QString componentName, components.keys())
        {
            totalComponents[componentName] = totalComponents.value(componentName, 0) +
                    (components.value(componentName) * blockCount);
        }
    }
    QVariantList list;
    foreach (QString componentName, totalComponents.keys())
    {
        QVariantMap listItem;
        listItem["name"] = componentName;
        listItem["amount"] = totalComponents.value(componentName);
        list.append(listItem);
    }

    return list;
}

bool ShipInfo::setShipFile(const QString &filename)
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        m_shipBlueprint.setXmlData(file.readAll());
        file.close();
    }
    emit shipLoaded();
}
