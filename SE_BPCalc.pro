TEMPLATE = app

QT += qml quick widgets xml

CONFIG += c++11

SOURCES += main.cpp \
    BlueprintListModel.cpp \
    Blueprints.cpp \
    CubeBlocks.cpp \
    ShipBlueprint.cpp \
    ShipInfo.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    BlueprintListModel.h \
    Blueprints.h \
    CubeBlocks.h \
    ShipBlueprint.h \
    ShipInfo.h

DISTFILES += \
    Components.json
