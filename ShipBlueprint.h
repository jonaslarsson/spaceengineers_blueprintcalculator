#ifndef SHIPBLUEPRINT_H
#define SHIPBLUEPRINT_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QByteArray>

class ShipBlueprint
{
public:
    ShipBlueprint();
    ~ShipBlueprint();

public slots:
    void setXmlData(const QByteArray& data);
    bool loadBlueprint(const QString& filename);
    QMap<QString, int> blocks() const;

private:
    QMap<QString, int> m_cubeBlocks;
};

#endif // SHIPBLUEPRINT_H
