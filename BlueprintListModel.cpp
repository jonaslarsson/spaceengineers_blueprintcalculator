#include <QDebug>
#include <QDir>
#include <QUrl>
#include <QFileInfo>
#include <QStandardPaths>
#include "BlueprintListModel.h"

struct BlueprintListModel::Blueprint
{
    enum
    {
        Local,
        Workshop
    } origin;
    QString name;
    QString bpFile;
    QString thumbsFile;
};

BlueprintListModel::BlueprintListModel() :
    m_blueprintDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/../SpaceEngineers/Blueprints")
{
}

BlueprintListModel::~BlueprintListModel()
{
    clear();
}

bool BlueprintListModel::refresh()
{
    if (m_blueprintDir.exists() == false)
    {
        return false;
    }

    QDir localDir(m_blueprintDir.absoluteFilePath("local"));
    foreach (QString blueprintName, localDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString thumbFile = localDir.absoluteFilePath(blueprintName) + "/thumb.png";
        QString bpFile = localDir.absoluteFilePath(blueprintName) + "/bp.sbc";
        if (QFile::exists(thumbFile) && QFile::exists(bpFile))
        {
            addLocalBlueprint(blueprintName);
        }
    }

    return true;
}

QHash<int, QByteArray> BlueprintListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[ImageUrlRole] = "imageUrl";
    roles[BlueprintFileRole] = "blueprintFile";
    return roles;
}

int BlueprintListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_blueprints.count();
}

QVariant BlueprintListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() >= m_blueprints.count()) return QVariant();
    Blueprint *blueprint = m_blueprints.at(index.row());

    switch (role)
    {
    case NameRole:
        return blueprint->name;
    case ImageUrlRole:
        return QUrl::fromLocalFile(blueprint->thumbsFile);
    case BlueprintFileRole:
        return blueprint->bpFile;
    }

    return QVariant();
}

void BlueprintListModel::clear()
{
    if (m_blueprints.count() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, m_blueprints.count() - 1);
        foreach (Blueprint *blueprint, m_blueprints)
        {
            delete blueprint;
        }
        m_blueprints.clear();
        m_blueprintsLocal.clear();
        endRemoveRows();
    }
}

// Add blueprint if it does not exist
void BlueprintListModel::addLocalBlueprint(QString blueprintName)
{
    if (m_blueprintsLocal.contains(blueprintName) == false)
    {
        beginInsertRows(QModelIndex(), m_blueprints.count(), m_blueprints.count());
        Blueprint *blueprint = new Blueprint;
        blueprint->origin = Blueprint::Local;
        blueprint->name = blueprintName;
        blueprint->bpFile = m_blueprintDir.absoluteFilePath("local") + "/" + blueprintName + "/bp.sbc";
        blueprint->thumbsFile = m_blueprintDir.absoluteFilePath("local") + "/" + blueprintName + "/thumb.png";
        m_blueprints.append(blueprint);
        m_blueprintsLocal.insert(blueprintName, blueprint);
        endInsertRows();
    }
}
