import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 1024
    height: 600
    title: qsTr("Space Engineers Blueprint Calculator")

    ScrollView {
        id: scrollView
        width: 96*3
        height: parent.height
        ListView {
            id: listView
            model: BlueprintModel
            highlightFollowsCurrentItem: false
            currentIndex: -1
            delegate: Item {
                width: parent.width
                height: 55
                Image {
                    y: 0
                    width: 96
                    height: 54
                    source: model.imageUrl
                }
                Text {
                    x: 105
                    width: parent.width - x
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    text: model.name
                    font.pixelSize: 15
                }
                Rectangle {
                    y: parent.height - 1
                    x: 0
                    width: parent.width
                    height: 1
                    color: "black"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (listView.currentIndex != index)
                        {
                            bigImage.source = model.imageUrl
                            listView.currentIndex = index
                            ShipInfo.setShipFile(model.blueprintFile)
                        }
                    }
                }
            }
            highlight: Rectangle {
                y: listView.currentItem.y
                width: parent.width
                height: listView.currentItem.height
                color: "lightsteelblue"
            }
        }
    }

    Item {
        x: scrollView.width
        width: parent.width - x
        height: parent.height

        Image {
            id: bigImage
            width: 960/2
            height: 540/2
            visible: false
        }

        PartsListView {
            x: 0
            model: ShipInfo.blocks
        }
        PartsListView {
            x: 200
            model: ShipInfo.parts
        }


//        Column {
//            Repeater {
//                model: ShipInfo.blocks
//                delegate: Text {
//                    height: 50
//                    width: 200
//                    text: ShipInfo.blocks[index].name
//                }
//            }
//        }

//        Rectangle {
//            width: 200
//            height: parent.height
//        }

    }

    //    menuBar: MenuBar {
    //        Menu {
    //            title: qsTr("File")
    //            MenuItem {
    //                text: qsTr("&Open")
    //                onTriggered: console.log("Open action triggered");
    //            }
    //            MenuItem {
    //                text: qsTr("Exit")
    //                onTriggered: Qt.quit();
    //            }
    //        }
    //    }

    //    MainForm {
    //        anchors.fill: parent
    //        button1.onClicked: messageDialog.show(qsTr("Button 1 pressed"))
    //        button2.onClicked: messageDialog.show(qsTr("Button 2 pressed"))
    //        listView1.model: BlueprintModel
    //    }

    //    MessageDialog {
    //        id: messageDialog
    //        title: qsTr("May I have your attention, please?")

    //        function show(caption) {
    //            messageDialog.text = caption;
    //            messageDialog.open();
    //        }
    //    }
}
