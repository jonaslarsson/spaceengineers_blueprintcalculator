#ifndef BLUEPRINTS_H
#define BLUEPRINTS_H

#include <QByteArray>
#include <QMap>

class Blueprints
{
public:
    Blueprints();
    ~Blueprints();
    void setXmlData(const QByteArray& data);
    QMap<QString, double> requiredIngots(const QString& componentName);

private:
    struct Blueprint;
    QList<Blueprint*> m_blueprints;
    QMap<QString, Blueprint*> m_components;
    void clear();
    void debugBlueprint(Blueprint *blueprint);
};

#endif // BLUEPRINTS_H
