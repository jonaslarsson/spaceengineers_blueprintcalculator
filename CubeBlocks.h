#ifndef CUBEBLOCKS_H
#define CUBEBLOCKS_H

#include <QString>
#include <QByteArray>

class CubeBlocks
{
public:
    CubeBlocks();
    ~CubeBlocks();
    void setXmlData(const QByteArray& data);
    QMap<QString, int> requiredComponents(const QString& blockName) const;

private:
    struct Definition;
    QList<Definition*> m_definitions;
    QMap<QString, Definition*> m_blocks;
    void clear();
//    void debugBlueprint(Blueprint *blueprint);
};

#endif // CUBEBLOCKS_H
